window.cockpit_po = {
 "": {
  "plural-forms": (n) => 0,
  "language": "ja",
  "language-direction": "ltr"
 },
 "Diagnostic reports": [
  null,
  "診断レポート"
 ],
 "Kernel dump": [
  null,
  "カーネルダンプ"
 ],
 "Networking": [
  null,
  "ネットワーキング"
 ],
 "SELinux": [
  null,
  "SELinux"
 ],
 "Storage": [
  null,
  "ストレージ"
 ]
};
