window.cockpit_po = {
 "": {
  "plural-forms": (n) => 0,
  "language": "zh_TW",
  "language-direction": "ltr"
 },
 "Diagnostic reports": [
  null,
  "診斷報告"
 ],
 "Kernel dump": [
  null,
  "核心傾印"
 ],
 "Networking": [
  null,
  "網路"
 ],
 "SELinux": [
  null,
  "SELinux"
 ],
 "Storage": [
  null,
  "儲存裝置"
 ]
};
