window.cockpit_po = {
 "": {
  "plural-forms": (n) => n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2,
  "language": "uk",
  "language-direction": "ltr"
 },
 "Diagnostic reports": [
  null,
  "Діагностичні звіти"
 ],
 "Kernel dump": [
  null,
  "Дамп ядра"
 ],
 "Networking": [
  null,
  "Робота у мережі"
 ],
 "SELinux": [
  null,
  "SELinux"
 ],
 "Storage": [
  null,
  "Сховище даних"
 ]
};
